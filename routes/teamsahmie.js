var express = require('express');
var router = express.Router();


// Require our controllers.
var teamsahmie_controller = require('../controllers/teamsahmieController'); 

// https://6bd9bf9ce60b47a6aa8de56ca048a9a5.vfs.cloud9.us-east-1.amazonaws.com/create
router.post('/create', teamsahmie_controller.create_post); 

// https://6bd9bf9ce60b47a6aa8de56ca048a9a5.vfs.cloud9.us-east-1.amazonaws.com/:user_id/destroy
router.get('/:user_id/destroy', teamsahmie_controller.delete_user_get); 

// https://6bd9bf9ce60b47a6aa8de56ca048a9a5.vfs.cloud9.us-east-1.amazonaws.com/:user_id/teamjides/create
router.post('/:user_id/teamsahmies/create', teamsahmie_controller.create_teamsahmie_post);

// https://6bd9bf9ce60b47a6aa8de56ca048a9a5.vfs.cloud9.us-east-1.amazonaws.com/:user_id/teamjides/:teamjide_id/destroy
router.get('/:user_id/teamsahmies/:team_id/destroy', teamsahmie_controller.delete_user_teamsahmie_get);

module.exports = router;