'use strict';
module.exports = (sequelize, DataTypes) => {
  var TeamSahmie = sequelize.define('TeamSahmie', {
    name: DataTypes.STRING
  });

  TeamSahmie.associate = function (models) {
    models.TeamSahmie.belongsTo(models.User, {
      onDelete: "CASCADE",
      foreignKey: {
        allowNull: false
      }
    });
  };

  return TeamSahmie;
};
